import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author asha achhami
 * @Date 6/27/2018
 **/
public class StudentMain {
    public static void main(String[] args) {
        ApplicationContext a= new ClassPathXmlApplicationContext("SpringConfig.xml");
        NameList n= (NameList)a.getBean("studentBean");
        n.display();

    }
}
